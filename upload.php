<?php
require ('functions.php');

echo "<a href='user_view.php'>Tagasi pealehele </a>";

$username = '';
$description = '';
$target_dir = "uploads/";

if (isset ( $_POST ['user_name'] )) {
	$username = $_POST ['user_name'];
	if (! file_exists ( $target_dir . $username . '/' )) {
		mkdir ( $target_dir . $username . '/', 0777, true );
	}
	$target_dir = $target_dir . $username . '/';
}
if (isset ( $_POST ['description'] )) {
	$description = $_POST ['description'];
}
$target_file = $target_dir . basename ( $_FILES ["fileToUpload"] ["name"] );
$uploadOk = 1;
$imageFileType = pathinfo ( $target_file, PATHINFO_EXTENSION );
// Check if image file is a actual image or fake image
if (isset ( $_POST ["submit"] )) {
	$check = getimagesize ( $_FILES ["fileToUpload"] ["tmp_name"] );
	if ($check !== false) {

		header ("Location: user_view.php");
		
		$uploadOk = 1;
	} else {
		header ("Location: add_post.php?alert=notImage");
		$uploadOk = 0;
	}
}
// Check if file already exists
if (file_exists ( $target_file )) {
	header ("Location: add_post.php?alert=fileExists");
	$uploadOk = 0;
}
// Check file size
if ($_FILES ["fileToUpload"] ["size"] > 50000000) {
	header ("Location: add_post.php?alert=tooLarge");
	$uploadOk = 0;
}
// Allow certain file formats
if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
	header ("Location: add_post.php?alert=notJpg");
	$uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
	header ("Location: add_post.php?alert=notUploaded");
	// if everything is ok, try to upload file
} else {
	if (move_uploaded_file ( $_FILES ["fileToUpload"] ["tmp_name"], $target_file )) {
		addImage ( $target_dir . $_FILES ["fileToUpload"] ["name"], $description );
		// db path to image $target_dir.$_FILES["fileToUpload"]["name"]
	} else {
		header ("Location: add_post.php?alert=garnooKurat");
	}
}
?>