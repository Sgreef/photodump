<?php
require ("functions.php");
$name = htmlspecialchars ( $_POST ['name'] );
$username = htmlspecialchars ( $_POST ['username'] );
$password = $_POST ['password'];
$pass = $_POST ['pass'];
$success = register ( $name, $username, $password, $pass );
if ($success !== true) {
	header ( "Location: register_view.php?alert=" . $success );
} else {
	header ( "Location: index.php" );
}
?>