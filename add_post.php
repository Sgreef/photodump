<?php require("functions.php") ?>
<!doctype html>
<html>

<head>
<meta charset="UTF-8">

<link rel="stylesheet" type="text/css" href="assetid/style.css">
<link rel="stylesheet" href="assetid/bootstrap/css/bootstrap.css">
<link rel="stylesheet" href="assets/css/font-awesome.min.css">
<link rel="shortcut icon" href="assetid/images/s.png">

<title>Projekt</title>


</head>

<body>
		<?php if (logged()) : ?>
<nav class="navbar navbar-default  navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">  
     <a class="navbar-brand" href="user_settings.php"><?= $_SESSION['name']?></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a class="lisa glyphicon glyphicon-picture" href="add_post.php"></a></li>
      </ul>
      <div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<a class="logo"  href="user_view.php" >Photodump</a>
					<ul class="nav navbar-nav navbar-right">
					<li><a id="logout" href="logout.php">Log out</a></li>
					</li>
				</ul>
			</div>
      </ul>
    </div>
  </div>
</nav>
<h1 class="postita">Postita pilt</h1>
<div class="upload">
	<div class="alert alert-danger <?= getAlert($_GET) == false? 'hidden' : ""?>"><?= getAlert($_GET)?></div>
		<form class="feed-info" action="upload.php" method="post"
			enctype="multipart/form-data">
			<input class="file form-control" type="file"
				name="fileToUpload" id="fileToUpload" required> 
			<input class="file form-control" type="text" name="description"
				placeholder="Kirjeldus" required> 
			<input class="form-control" type="hidden" name="user_name"
				value="<?php echo($_SESSION['username']) ?>"> 
			<input class="upload-nupp btn btn-primary" type="submit"
				value="Upload Image" name="submit">
		</form>
		
		</div>
		
        <?php else : ?>
        <?php header('Location: index.php')?>
	</body>
<?php endif ?>
</html>