<?php require("functions.php")?>
<!doctype html>
<html>

<head>
<link rel="stylesheet" type="text/css" href="assetid/style.css">
<link rel="stylesheet" href="assetid/bootstrap/css/bootstrap.css">
<link rel="stylesheet" href="assets/css/font-awesome.min.css">
<link rel="shortcut icon" href="assetid/images/s.png">
<meta content="width = device-width, initial-scale = 1" name="viewport">
<meta charset="UTF-8">
<title>Projekt</title>


</head>

<body>
	<?php if (logged()) : ?>
      You are logged in. <a href="logout.php">log out</a>
    <?php else : ?>
		<div class="col-xs-6 ">
		<div class="esimene">
			<img id="sletter" src="assetid/images/s.png">
			<form class="vorm" method="POST" action="login.php">

				<table class="tabel">

					<tr>
						<td><input class="form-control inputs" type="text" name="username"
							placeholder="Kasutajanimi" required></td>
					</tr>

					<tr>
						<td><input class="form-control inputs" type="password"
							name="password" placeholder="Parool" required></td>
					</tr>


					<tr>
						<td colspan="2" id="loginupud">
							<button class="btn btn-success" type="submit">Logi sisse</button>
							<a href="register_view.php" class="btn btn-danger">Registreeru</a>
							<div id="minualert"
								class="alert alert-danger <?= getAlert($_GET) == false? 'hidden' : ""?>"><?= getAlert($_GET)?></div>
						</td>
					</tr>

				</table>
			</form>
		</div>
	</div>
	<div class="teine col-xs-6">
		<h1>Tere tulemast keskkonda!</h1>

	</div>
         <?php endif;	?></body>

</html>