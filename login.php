<?php
require ("functions.php");

$username = htmlspecialchars ( $_POST ['username'] );
$password = $_POST ['password'];
$success = login ( $username, $password );
if ($success !== true) {
	header ( "Location: index.php?alert=" . $success );
} else {
	header ( "Location: user_view.php" );
}
?>