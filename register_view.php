<?php require("functions.php") ?>
<!doctype html>
<html>

<head>
<link rel="stylesheet" type="text/css" href="assetid/style.css">
<link rel="stylesheet" href="assetid/bootstrap/css/bootstrap.css">
<link rel="stylesheet" href="assets/css/font-awesome.min.css">
<link rel="shortcut icon" href="assetid/images/s.png">
<meta charset="utf-8">
<title>Registreeru</title>


</head>

<body>
		<?php if (logged()) : ?>
	    <?php header("Location: index.php") ?>
	    <?php else : ?>
	    <div class="regavorm">
		<h1 class="rega">Registreeru</h1>
		<form class="regatabel" action="register.php" method="post">

			<table>

				<tr>
					<td><input class="form-control inputs" type="text" name="name"
						placeholder="Ees- ja Perekonnanimi" required></td>
				</tr>
				<tr>
					<td><input class="form-control inputs" type="text" name="username"
						placeholder="Kasutajanimi" required></td>
				</tr>

				<tr>
					<td><input class="form-control inputs" type="password" name="pass"
						placeholder="Parool" required></td>
				</tr>
				<tr>
					<td><input class="form-control inputs" type="password"
						name="password" placeholder="Korda parooli" required></td>
				</tr>


				<tr>
					<td>
						<button id="reganupp" type="submit" class="btn btn-danger">Registreeru</button>
					</td>
				</tr>
			</table>
		</form>
		
		<div class="regaalert alert alert-danger <?= getAlert($_GET) == false? 'hidden' : ""?>"><?= getAlert($_GET)?></div>
	
	</div>


<?php endif ?>
</body>

</html>