<?php
define ( "HOST", "localhost" );
define ( "USER", "test" );
define ( "PASS", "t3st3r123" );
define ( "DB", "test" );
function getDatabaseConnection() {
	$con = mysqli_connect ( HOST, USER, PASS, DB );
	if ($con->connect_error) {
		die ( "Connection failed: " . $con->connect_error );
	} else {
		return $con;
	}
}
function register($name, $username, $password, $pass) {
	if (strlen($password) < 8 || strlen($username) < 2  || strlen($name) < 2){
		return "invalidData";
	} elseif ($pass !== $password){
		return "invalidPass";	
	} elseif(isUnique($username) == true) {
		$con = getDatabaseConnection ();
		
		$sql = "INSERT INTO sgreef_usertable (nimi, kasutajatunnus, parool) VALUES (?, ?, ?);";
		$query = $con->prepare ( $sql );
		$hash = password_hash ( $password, PASSWORD_DEFAULT );
		$query->bind_param ( 'sss', $name, $username, $hash );
		$query->execute ();
		header ( "Location: index.php" );
		
		return true;
	} else {
		return "userExists";
	}
	$con->close ();
}
function isUnique($username){
	$con = getDatabaseConnection ();
	$sql = "SELECT count(*) FROM sgreef_usertable WHERE kasutajatunnus = ?;";
	
	$query = $con->prepare ( $sql );
	$query->bind_param('s', $username);
	$query->execute ();
	
	$result = $query->get_result ();
	$row = $result->fetch_row ();
	
	if ($row[0] != 0) {
        return false;
      } else {
        return true;
      }
     
      $con->close();
	}	
function login($username, $password) {
	startSession ();
	$con = getDatabaseConnection ();
	
	$sql = "SELECT id, nimi, kasutajatunnus, parool FROM sgreef_usertable WHERE kasutajatunnus = ? LIMIT 1;";
	$query = $con->prepare ( $sql );
	$query->bind_param ( 's', $username );
	$query->execute ();
	
	$result = $query->get_result ();
	$row = $result->fetch_assoc ();
	
	$con->close ();
	
	if (isset ( $row ) && password_verify ( $password, $row ["parool"] )) {
		$_SESSION ['id'] = preg_replace ( "/[^0-9]+/", "", $row ['id'] );
		$_SESSION ['name'] = $row ['nimi'];
		$_SESSION ['username'] = preg_replace ( "/[^a-zA-Z0-9_\-]+/", "", $row ["kasutajatunnus"] );
		$_SESSION ['password'] = $row ["parool"];
		return true;
	}
	return "wrongCredentials";
}
function logged() {
	startSession ();
	if (isset ( $_SESSION ['id'], $_SESSION ['name'], $_SESSION ['username'], $_SESSION ['password'] )) {
		$id = $_SESSION ['id'];
		$name = $_SESSION ['name'];
		$username = $_SESSION ['username'];
		$password = $_SESSION ['password'];
		
		$con = getDatabaseConnection ();
		
		$sql = "SELECT parool FROM sgreef_usertable WHERE id = ? LIMIT 1;";
		$query = $con->prepare ( $sql );
		$query->bind_param ( 'i', $id );
		$query->execute ();
		
		$result = $query->get_result ();
		$row = $result->fetch_assoc ();
		if (isset ( $row )) {
			if ($password == $row ["parool"]) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	} else {
		return false;
	}
}
function startSession() {
	$session_name = 'sessiontime';
	$secure = false;
	$httponly = true;
	$cookieParams = session_get_cookie_params ();
	session_set_cookie_params ( $cookieParams ["lifetime"], $cookieParams ["path"], $cookieParams ["domain"], $secure, $httponly );
	session_name ( $session_name );
	session_start ();
	session_regenerate_id ( true );
}
function logout() {
	startSession ();
	$_SESSION = array ();
	$params = session_get_cookie_params ();
	setcookie ( session_name (), '', time () - 42000, $params ["path"], $params ["domain"], $params ["secure"], $params ["httponly"] );
	session_destroy ();
}
function postComment($comment, $postId) {
	startSession ();
	$con = getDatabaseConnection ();
	$poster = $_SESSION ['name'];
	$sql = "INSERT INTO sgreef_comments (comment, user, picId) VALUES (?, ?, ?);";
	$query = $con->prepare ( $sql );
	$query->bind_param ( 'ssi', $comment, $poster, $postId );
	$query->execute ();
	header ( "Location: user_view.php" );
	
	$con->close ();
}
function getComments($postId) {
	$con = getDatabaseConnection ();
	$sql = "SELECT comment, user FROM sgreef_comments WHERE picId = ?;";
	
	$query = $con->prepare ( $sql );
	$query->bind_param ( 'i', $postId );
	$query->execute ();
	
	$result = $query->get_result ();
	
	$rows = array ();
	while ( $row = $result->fetch_assoc () ) {
		$rows [] = $row;
	}
	$con->close ();
	return $rows;
}
function getAlert($getData) {
	if (isset ( $getData ['alert'] )) {
		switch ($getData ['alert']) {
			case "wrongCredentials" :
				$alert = "Vale parool või kasutajatunnus.";
				break;
			case "notLoggedIn" :
				$alert = "You need to log in first.";
				break;
			case "userExists" :
				$alert = "Sellise kasutajanimega kasutaja on juba olemas.";
				break;
			case "invalidData" :
				$alert = "Sisestatud andmed on valed.";
				break;
			case "invalidPass" :
				$alert = "Sisestatud paroolid ei klapi!";
				break;
			case "notImage" :
				$alert = "Antud fail pole pilt.";
				break;
			case "fileExists" :
				$alert = "Sellise nimega fail on juba olemas.";
				break;
			case "tooLarge" :
				$alert = "Valitud fail on liiga suur.";
				break;	
			case "notJpg" :
				$alert = "Ainult JPG, JPEG, PNG & GIF tüüpi failid on lubatud.";
				break;
			case "notUploaded" :
				$alert = "Fail ei laetud üles.";
				break;					
			default :
				$alert = "Tekkis viga.";
				break;
		}
		return $alert;
	} else {
		return false;
	}
}
function getNewPostId() {
	$con = getDatabaseConnection ();
	$sql = "SELECT count(*) FROM sgreef_posts";
	
	$query = $con->prepare ( $sql );
	$query->execute ();
	
	$result = $query->get_result ();
	$row = $result->fetch_assoc ();
	
	$con->close ();
	
	return intval ( $row ['count(*)'] );
}
function addPost() {
	startSession ();
	
	$con = getDatabaseConnection ();
	$sql = "INSERT INTO sgreef_posts (userId, heading) VALUES (?, ?);";
	
	$query = $con->prepare ( $sql );
	$query->bind_param ( 'is', $_SESSION ['id'], 'TERE' );
	$query->execute ();
	
	$con->close ();
}
function getPosts($id = null) {
	$con = getDatabaseConnection ();
	
	if ($id != null) {
		$sql = "SELECT id, userId, heading, path FROM sgreef_posts WHERE userId = ? order by id desc;";
		$query = $con->prepare ( $sql );
		$query->bind_param ( 'i', $id );
	} else {
		$sql = "SELECT id, userId, heading, path FROM sgreef_posts order by id desc;";
		$query = $con->prepare ( $sql );
	}
	
	$query->execute ();
	$result = $query->get_result ();
	
	$rows = array ();
	while ( $row = $result->fetch_assoc () ) {
		$rows [] = $row;
	}
	$con->close ();
	return $rows;
}
function deletePost($postId) {
	$con = getDatabaseConnection ();
	
	$sql = "DELETE FROM sgreef_posts WHERE id = ?;";
	
	$query = $con->prepare ( $sql );
	$query->bind_param ( 'i', $postId );
	$query->execute ();
	
	$con->close ();
}
function getName($userId) {
	$con = getDatabaseConnection ();
	$sql = "SELECT nimi FROM sgreef_usertable WHERE id = ? LIMIT 1;";
	
	$query = $con->prepare ( $sql );
	$query->bind_param ( 'i', $userId );
	$query->execute ();
	
	$result = $query->get_result ();
	$row = $result->fetch_assoc ();
	
	$con->close ();
	
	return $row ['nimi'];
}
function addImage($image_url, $description) {
	startSession ();
	
	$con = getDatabaseConnection ();
	$sql = "INSERT INTO sgreef_posts (userId, heading, path) VALUES (?, ?, ?)";
	
	$query = $con->prepare ( $sql );
	$query->bind_param ( 'iss', $_SESSION ['id'], $description, $image_url );
	$query->execute ();
	
	$con->close ();
}
?>