<?php require("functions.php") ?>
<!doctype html>
<html>

<head>
<meta charset="UTF-8">

<link rel="stylesheet" type="text/css" href="assetid/style.css">
<link rel="stylesheet" href="assetid/bootstrap/css/bootstrap.css">
<link rel="stylesheet" href="assets/css/font-awesome.min.css">
<link rel="shortcut icon" href="assetid/images/s.png">

<title>Projekt</title>


</head>

<body>
		<?php if (logged()) : ?>
<nav class="navbar navbar-default  navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">  
     <a class="navbar-brand" href="user_settings.php"><?= $_SESSION['name']?></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a class="lisa glyphicon glyphicon-picture" href="add_post.php"></a></li>
      </ul>
      <div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<a class="logo">Photodump</a>
					<ul class="nav navbar-nav navbar-right">
					<li><a id="logout" href="logout.php">Log out</a></li>
					</li>
				</ul>
			</div>
      </ul>
    </div>
  </div>
</nav>

	<div class="feed">
		<?php $data = getPosts(); ?>
			<div class="feed-column comment-section">
				<?php foreach ($data as $pic) : ?>
				<h2 id="postitaja">Postitaja: <?php echo getName($pic['userId'])?> </h2>
			<img src="<?php echo $pic['path'] ?>"
				class="feed-image img-thumbnail">
			<h4> <?php echo $pic['heading']?> </h4>
				<?php foreach (getComments($pic['id']) as $comment) : ?>

				<p>
				<span id="kommenteeria"><?php echo $comment['user']?></span id="kommenteeria"><span>  :  </span><span><?php echo htmlspecialchars ($comment['comment'])?></span>
			</p>
				<?php endforeach ?>
				<form id="komenteerimisala" action=comment.php method="POST">
				<div class="comment-post">
					<input type="text" class="form-control" name="comment"
						placeholder="Kommentaar">
					<button type="submit" class="btn btn-primary">Postita</button>
					<input type="hidden" name="postId" value="<?= $pic['id'] ?>">
				</div>
			</form>
				<?php endforeach ?>
			</div>

	</div>
        <?php else : ?>
        <?php header('Location: index.php')?>
	</body>
<?php endif ?>
</html>