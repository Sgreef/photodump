<?php require("functions.php") ?>
<!doctype html>
<html>

<head>
<link rel="stylesheet" type="text/css" href="assetid/style.css">
<link rel="stylesheet" href="assetid/bootstrap/css/bootstrap.css">
<link rel="stylesheet" href="assets/css/font-awesome.min.css">
<link rel="shortcut icon" href="assetid/images/s.png">
<meta charset="utf-8">
<title>Projekt</title>


</head>

<body>
		
		<?php if (logged()) : ?>
			<nav class="navbar navbar-default  navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">  
     <a class="navbar-brand" href="user_settings.php"><?= $_SESSION['name']?></a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a class="lisa glyphicon glyphicon-picture" href="add_post.php"></a></li>
      </ul>
      <div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<a class="logo" href="user_view.php">Photodump</a>
					<ul class="nav navbar-nav navbar-right">
					<li><a id="logout" href="logout.php">Log out</a></li>
					</li>
				</ul>
			</div>
      </ul>
    </div>
  </div>
</nav>
	<div class="feed">
	<h1 class="sinu">Sinu pildid</h1>
		<?php $data = getPosts($_SESSION['id']); ?>
		<?php if(empty($data)):?>
			<h2>Sul pole veel ühtegi postitust.</h2>
			<h2>Pildi lisamiseks vajuta menüüribal pildi ikooni.</h2>
			<?php else :?>
			<div class="feed-column comment-section">
				<?php foreach ($data as $pic) : ?>
				<h2 id="postitaja">Postitaja: <?php echo getName($pic['userId'])?></h2>
				<a class="x glyphicon glyphicon-remove" href="deletePost.php?postId=<?=$pic['id']?>"></a>
			<img src="<?php echo $pic['path'] ?>"
				class="feed-image img-thumbnail">
			<h4> <?php echo $pic['heading']?> </h4>
				<?php foreach (getComments($pic['id']) as $comment) : ?>
				<p><span id="kommenteeria"><?php echo $comment['user']?></span>: <span><?php echo htmlspecialchars ($comment['comment'])?></span></p>
				<?php endforeach ?>
				<form id="komenteerimisala" action=comment.php method="POST">
				<div class="comment-post">
					<input type="text" class="form-control" name="comment"
						placeholder="Kommentaar">
					<button type="submit" class="btn btn-primary">Postita</button>
					<input type="hidden" name="postId" value="<?= $pic['id'] ?>">
				</div>
			</form>
				<?php endforeach ?>
			</div>

	</div>

		<?php endif ?>


        <?php else : ?>
        <?php header('Location: index.php')?>
	</body>
<?php endif ?>
</html>